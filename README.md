# Middleware Schemas

Contains schemas used by Middleware projects.

## Usage
Schemas are located at:

https://git.it.vt.edu/middleware/schema/raw/master

